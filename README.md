# SXA Site Resolver Issue

This is a project to demonstrate issues caused by the SXA Site Resolver when
`Enable Preview` is not checked on a site definition.

The root cause of all of the issues is that the `Sites` property on the
`Sitecore.XA.Foundation.Multisite.SiteInfoResolver` class filters out sites
with `Enable Preview` unchecked.

## Setup

1. Install a new instance of Sitecore 9.0 Update-1.
2. Install `Sitecore PowerShell Extensions-4.7.2 for Sitecore 8.zip` in
   Sitecore.
3. Install `Sitecore Experience Accelerator 1.7 rev. 180410 for 9.0.zip` in
   Sitecore.
4. Update the `publishUrl` in the [publish profile][1] to point to your Sitecore
   installation.
5. Update the `physicalRootPath` in
   [Unicorn.CustomSerializationFolder.config][2] to point to the correct path on
   your disk.
6. Publish the solution.
7. Sync Unicorn at `/unicorn.aspx`.
8. **Sync Unicorn again (important!).**
9. Perform a smart site publish.

## Testing the issue

There are two site groupings set up for the test site: `Content Management` and
`Content Delivery`. The `Content Management` site grouping has `Enable Preview`
checked; the `Content Delivery` site grouping has `Enable Preview` unchecked.

To toggle between the two site groupings, open [SxaSiteResolverIssue.config][3]
and set `XA.Foundation.Multisite.Environment` to either `ContentManagement` or
`ContentDelivery` and publish the solution to activate the `Content Management`
or `Content Delivery` site grouping, respectively.

With the `Content Management` site grouping active, navigate to the root of the
site and observe the following:

1. The `Navigation` component renders.
2. The links in the `Link List` component are all correct and work.
3. The `Site List` component lists the `Test Site`.
4. The `Internal Link` text in the `Site List` component renders the proper
   relative URL for `Page 1`.
5. The value of the `sxa_site` cookie is correct: `Test Site`.
6. The `Server Error` page content is rendered when you click on the
   `Page with Error` link (i.e., navigate to `/page-with-error`).

With the `Content Delivery` site grouping active, observe:

1. The `Navigation` component doesn't render at all.
2. The links in the `Link List` all have the full item path and do not work.
3. The `Site List` component does not list the `Test Site`
   (the `SiteInfoResolver` is filtering it out).
4. The `Internal Link` text in the `Site List` component renders the full item
   path as the URL for `Page 1` (`LinkItem.TargetUrl` does not work).
5. The value of the `sxa_site` cookie is incorrect: `website`.
6. An ASP.NET error page is shown when you click on the `Page with Error` link
   (the SXA server error handling does not work).

This is surely not an exhaustive list--any code that depends on the `Sites`
property of `Sitecore.XA.Foundation.Multisite.SiteInfoResolver` will have
similarly buggy behavior when `Enable Preview` is disabled (e.g., on a content
delivery server).

Another interesting observation is that the site-relative tokens (e.g.,
`query:$site`) do not work if `Enable Preview` is unchecked. To test, open the
`Content Management` site grouping under the `Test Site` node and uncheck
`Enable Preview`. Open the `Site List 1` data source under `Home/Data`. Click
`Insert link` on the `Internal Link` field and observe that none of the
`Test Site` items are shown despite that field's source being `query:$site`.
Check `Enable Preview` in the site grouping and observe that it shows items for
the `Test Site` site.

[1]: src/SxaSiteResolverIssue.Web/Properties/PublishProfiles/PublishProfile.pubxml
[2]: src/SxaSiteResolverIssue.Web/App_Config/Include/zzz.SxaSiteResolverIssue/Unicorn.CustomSerializationFolder.config
[3]: src/SxaSiteResolverIssue.Web/App_Config/Include/zzz.SxaSiteResolverIssue/SxaSiteResolverIssue.config
﻿using System.Collections.Generic;
using Sitecore.Web;
using Sitecore.XA.Foundation.Mvc.Models;

namespace SxaSiteResolverIssue.Web.Models
{
  public class SiteListRenderingModel : RenderingModelBase
  {
    public IEnumerable<SiteInfo> Sites { get; set; }
    public string InternalLink { get; set; }
  }
}

﻿using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using SxaSiteResolverIssue.Web.Controllers;
using SxaSiteResolverIssue.Web.Repositories;

namespace SxaSiteResolverIssue.Web
{
  public class SxaSiteResolverIssueConfigurator : IServicesConfigurator
  {
    public void Configure(IServiceCollection serviceCollection)
    {
      serviceCollection.AddTransient<SiteListController>();
      serviceCollection.AddTransient<ISiteListRepository, SiteListRepository>();
    }
  }
}

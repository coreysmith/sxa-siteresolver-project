﻿using System;
using Sitecore.XA.Foundation.Multisite;
using Sitecore.XA.Foundation.Multisite.LinkManagers;
using Sitecore.XA.Foundation.Mvc.Repositories.Base;
using SxaSiteResolverIssue.Web.Models;

namespace SxaSiteResolverIssue.Web.Repositories
{
  public class SiteListRepository : ModelRepository, ISiteListRepository
  {
    private readonly ISiteInfoResolver _siteInfoResolver;

    public SiteListRepository(ISiteInfoResolver siteInfoResolver)
    {
      _siteInfoResolver = siteInfoResolver ?? throw new ArgumentNullException(nameof(siteInfoResolver));
    }

    public override IRenderingModelBase GetModel()
    {
      var model = new SiteListRenderingModel();
      FillBaseProperties(model);

      model.Sites = _siteInfoResolver.Sites;
      model.InternalLink = LinkItem.FromField(Rendering.DataSourceItem, false, Templates.SiteList.Fields.InternalLink)?.TargetUrl;

      return model;
    }
  }
}

﻿using System;
using Sitecore.XA.Foundation.Mvc.Controllers;
using SxaSiteResolverIssue.Web.Repositories;

namespace SxaSiteResolverIssue.Web.Controllers
{
  public class SiteListController : StandardController
  {
    private readonly ISiteListRepository _siteListRepository;

    public SiteListController(ISiteListRepository siteListRepository)
    {
      _siteListRepository = siteListRepository ?? throw new ArgumentNullException(nameof(siteListRepository));
    }

    protected override object GetModel()
    {
      return _siteListRepository.GetModel();
    }
  }
}
